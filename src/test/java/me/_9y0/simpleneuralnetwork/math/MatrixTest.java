package me._9y0.simpleneuralnetwork.math;

import org.junit.Assert;
import org.junit.Test;

import nl.makertim.functionalmaker.AssertTry;

public class MatrixTest {

	@Test
	public void testConstructor() {
		AssertTry.assumeThrows(() -> new Matrix(2, 2, new double[2]), IllegalArgumentException.class);
		AssertTry.assumeThrows(() -> new Matrix(3, 5, new double[14]), IllegalArgumentException.class);

		AssertTry.assumeSave(() -> new Matrix(1, 4, new double[4]));
		AssertTry.assumeSave(() -> new Matrix(6, 4, new double[24]));
	}

	@Test
	public void testEquals() {
		Matrix a = new Matrix(2, 2, new double[]{5, 5, 5, 5});
		Matrix b = new Matrix(2, 2, new double[]{5, 5, 5, 5});
		Matrix c = new Matrix(2, 1, new double[]{1, 1});
		Matrix d = new Matrix(1, 2, new double[]{1, 1});

		Assert.assertEquals("a isn't equal to itself", a, a);
		Assert.assertNotEquals("a is equal to null", a, null);

		Assert.assertNotEquals("a is equal to c", a, c);
		Assert.assertNotEquals("a is equal to d", a, d);

		Assert.assertEquals("b isn't equal to a", a, b);
		Assert.assertEquals("hash of b isn't equal to hash of a", a.hashCode(), b.hashCode());
	}

	@Test
	public void testClone() {
		Matrix a = new Matrix(2, 2, new double[]{5, 5, 5, 5});
		Matrix b = new Matrix(a);

		Assert.assertEquals("b isn't equal to a", a, b);

		Matrix c = new Matrix(2, 2, new double[]{5, 5, 5, 5});
		Matrix d = new Matrix(a).add(5);

		Assert.assertNotEquals("b is equal to a", c, d);
	}

	@Test
	public void testMap() {
		Matrix a = new Matrix(2, 1, new double[]{5, -5});
		Matrix b = a.map(currentValue -> Math.max(0, currentValue));

		Assert.assertEquals("Mapping matrix didn't work", new Matrix(2, 1, new double[]{5, 0}), b);
	}

	@Test
	public void testRandomize() {
		Matrix matrix = new Matrix(2, 2).randomize();

		Assert.assertNotEquals("Random matrix only has 0 as value", matrix, new Matrix(2, 2));
	}

	@Test
	public void testNumberMultiply() {
		Matrix a = new Matrix(2, 2, new double[]{1, 2, 3, 4});
		Matrix b = a.multiply(5);

		Assert.assertEquals("a and 5 don't multiply right", new Matrix(2, 2, new double[]{5, 10, 15, 20}), b);
	}

	@Test
	public void testMatrixMultiply() {
		Matrix a = new Matrix(3, 2);
		Matrix b = new Matrix(3, 4);

		AssertTry.assumeThrows(() -> a.multiply(b), IllegalArgumentException.class);

		Matrix c = new Matrix(2, 3, new double[]{1, 2, 3, 4, 5, 6});
		Matrix d = new Matrix(3, 2, new double[]{1, 2, 3, 4, 5, 6});
		Matrix e = c.multiply(d);

		Assert.assertEquals("a and b don't multiply right",
				new Matrix(3, 3, new double[]{9, 12, 15, 19, 26, 33, 29, 40, 51}), e);
	}

	@Test
	public void testTranspose() {
		Matrix a = new Matrix(2, 3, new double[]{1, 2, 3, 4, 5, 6});
		Matrix b = a.transpose();

		Assert.assertEquals("b isn't transposed a", new Matrix(3, 2, new double[]{1, 3, 5, 2, 4, 6}), b);
	}

	@Test
	public void testNumberAdd() {
		Matrix a = new Matrix(2, 2, new double[]{1, 2, 3, 4});
		Matrix b = a.add(5.4);

		Assert.assertEquals("a and 5.4 don't add up together", new Matrix(2, 2, new double[]{6.4, 7.4, 8.4, 9.4}), b);
	}

	@Test
	public void testMatrixAdd() {
		AssertTry.assumeThrows(() -> {
			Matrix a = new Matrix(2, 2, new double[]{2, 2, 2, 2});
			Matrix b = new Matrix(3, 2, new double[]{1, 2, 3, 4, 5, 6});

			a.add(b);
		}, IllegalArgumentException.class);

		AssertTry.assumeThrows(() -> {
			Matrix a = new Matrix(2, 3, new double[]{1, 2, 3, 4, 5, 6});
			Matrix b = new Matrix(3, 2, new double[]{1, 2, 3, 4, 5, 6});

			a.add(b);
		}, IllegalArgumentException.class);

		Matrix a = new Matrix(3, 2, new double[]{1, 2, 3, 4, 5, 6});
		Matrix b = new Matrix(3, 2, new double[]{1, 2, 3, 4, 5, 6});
		Matrix c = a.add(b);

		Assert.assertEquals("a and b don't add up together", new Matrix(3, 2, new double[]{2, 4, 6, 8, 10, 12}), c);
	}
}
