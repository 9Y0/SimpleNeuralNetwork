package me._9y0.simpleneuralnetwork.math;

import java.util.Arrays;
import java.util.Random;
import java.util.function.DoubleUnaryOperator;

import nl.makertim.functionalmaker.functionalinterface.TriFunction;

public class Matrix {

	private static final Random RANDOM = new Random();

	private double[] data;
	private int columns;
	private int rows;

	public Matrix(int columns, int rows) {
		this(columns, rows, new double[columns * rows]);
	}

	public Matrix(Matrix other) {
		this(other.columns, other.rows, Arrays.copyOf(other.data, other.data.length));
	}

	public Matrix(int columns, int rows, double[] data) {
		if (data.length != rows * columns) {
			throw new IllegalArgumentException("columns * rows is not equal to the length of the array");
		}

		this.columns = columns;
		this.rows = rows;
		this.data = data;
	}

	public Matrix map(DoubleUnaryOperator function) {
		return this.map((x, y, currentValue) -> function.applyAsDouble(currentValue));
	}

	/**
	 * @param function
	 *            Parameters: x, y, currentValue
	 */
	public Matrix map(TriFunction<Integer, Integer, Double, Double> function) {
		for (int x = 0; x < columns; x++) {
			for (int y = 0; y < rows; y++) {
				data[x + y * columns] = function.apply(x, y, data[x + y * columns]);
			}
		}

		return this;
	}

	public Matrix multiply(double number) {
		return map(currentValue -> currentValue * number);
	}

	public Matrix multiply(Matrix other) {
		if (!canMultiply(other)) {
			throw new IllegalArgumentException(this + " and " + other + " can't be multiplied.");
		}
		Matrix newMatrix = new Matrix(other.columns, this.rows);

		newMatrix.map((x, y, currentValue) -> {
			double[] row = this.getRow(y);
			double[] column = other.getColumn(x);

			return sumOfRowAndColumn(row, column);
		});

		return newMatrix;
	}

	private boolean canMultiply(Matrix other) {
		return this.columns == other.rows;
	}

	private double sumOfRowAndColumn(double[] row, double[] column) {
		double sum = 0.0;

		for (int i = 0; i < row.length; i++) {
			sum += row[i] * column[i];
		}

		return sum;
	}

	private double[] getRow(int index) {
		double[] row = new double[this.columns];

		System.arraycopy(this.data, index * this.columns, row, 0, this.columns);

		return row;
	}

	private double[] getColumn(int index) {
		double[] column = new double[this.rows];

		for (int i = 0; i < this.rows; i++) {
			column[i] = this.data[index + i * this.columns];
		}

		return column;
	}

	public Matrix transpose() {
		Matrix old = new Matrix(this);

		this.columns = old.rows;
		this.rows = old.columns;

		return this.map((x, y, currentValue) -> old.data[y + x * old.columns]);
	}

	public Matrix add(double toAdd) {
		return map(currentValue -> currentValue + toAdd);
	}

	public Matrix add(Matrix other) {
		if (!canAdd(other)) {
			throw new IllegalArgumentException("Sizes of this " + this + " and other " + other + " aren't the same.");
		}

		return this.map((x, y, currentValue) -> currentValue + other.data[x + y * columns]);
	}

	private boolean canAdd(Matrix other) {
		return this.columns == other.columns && this.rows == other.rows;
	}

	public Matrix randomize() {
		return map(currentValue -> RANDOM.nextDouble() * 2 - 1);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Matrix matrix = (Matrix) o;

		if (columns != matrix.columns)
			return false;
		if (rows != matrix.rows)
			return false;
		return Arrays.equals(data, matrix.data);
	}

	@Override
	public int hashCode() {
		int result = Arrays.hashCode(data);
		result = 31 * result + columns;
		result = 31 * result + rows;
		return result;
	}

	@Override
	public String toString() {
		return String.format("Matrix{%n\tcolumns=%s,rows=%s,data={%n%s\t}%n}", columns, rows, dataToString());
	}

	private String dataToString() {
		StringBuilder sb = new StringBuilder();

		for (int y = 0; y < rows; y++) {
			sb.append("\t");
			for (int x = 0; x < columns; x++) {
				sb.append("\t").append(data[x + y * columns]);
			}
			sb.append("\n");
		}

		return sb.toString();
	}
}
